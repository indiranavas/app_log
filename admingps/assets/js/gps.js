    var map;
    var contentString;
    var infowindows = [];
    var markers = [];
    var poli = {};
    var poli_destino = {};
    var geopoli = [];
    var geopoli_destino = [];

    function ajaxShowMapa(vUrl,vNum,vModal,vEmpresa){
        Stopinterval(vModal);
        
        if($.browser.version > 8 && $.browser.version <= 11 && $.browser.mozilla)
        {
            var myDiv = $('#'+vModal+'Aux');
    
            if (myDiv.length){
                vModal += 'Aux';
            }        
        }

        poli = getPosicionPoligono(vUrl,vNum);
        geopoli = getGeocercaPoligono(vUrl,vNum);
        $.ajax({
            url: vUrl+'general/getdatogps',
            type: 'POST',
            data: {poligono: vNum, empresa: vEmpresa, poligono_destino: ''},
            dataType: 'json',
            beforeSend: function () {
              $("#"+vModal+" #cuerpomodal").html('<img src="'+vUrl+'assets/images/input-spinner.gif">');
              $("#"+vModal).modal("show");
            },
            success: function (resp) {
              setTimeout(function() { initMap(resp,vModal,'cuerpomodal'); }, 500); 
            }
        });  

    }

    function ajaxGetMapa(vUrl,vNum,vDiv,vEmpresa,vPoliDestino){
      poli = getPosicionPoligono(vUrl,vNum);
      geopoli = getGeocercaPoligono(vUrl,vNum);

      poli_destino = getPosicionPoligono(vUrl,vPoliDestino);
      geopoli_destino = getGeocercaPoligono(vUrl,vPoliDestino);

      $.ajax({
            url: vUrl+'general/getdatogps',
            type: 'POST',
            data: {poligono: vNum, empresa: vEmpresa, poligono_destino: vPoliDestino},
            dataType: 'json',
            beforeSend: function () {
              $("#"+vDiv+"").html('<img src="'+vUrl+'assets/images/input-spinner.gif">');
            },
            success: function (resp) {
              setTimeout(function() { initMap(resp,vDiv,''); }, 500); 
            }
        });  
    }

    function ajaxMapa(vUrl,vDiv,vEmpresa,vPatente){
      //poli = getPosicionPoligono(vUrl,1);
      //geopoli = getGeocercaPoligono(vUrl,1);
      $.ajax({
            url: vUrl+'general/getdatogpspatente',
            type: 'POST',
            data: {empresa: vEmpresa, patente: vPatente},
            dataType: 'json',
            beforeSend: function () {
              $("#"+vDiv+"").html('<img src="'+vUrl+'assets/images/input-spinner.gif">');
            },
            success: function (resp) {
              setTimeout(function() { initMap(resp,vDiv,''); }, 500); 
            }
        });  
    }


    function getPosicionPoligono(vUrl,vPoli){
      var salida = {};
        $.ajax({
            url: vUrl+'general/getposicionpoligono',
            type: 'POST',
            async: false, 
            data: {poligono: vPoli},
            dataType: 'json',
            success: function (resp) {
                salida = resp;
            }
        });  
        return salida;
    }

    function getGeocercaPoligono(vUrl,vPoli){
      var salida = [];
        $.ajax({
            url: vUrl+'general/getgeocercapoligono',
            type: 'POST',
            async: false, 
            data: {poligono: vPoli},
            dataType: 'json',
            success: function (resp) {
              salida = resp;
            }
        });  
        return salida;
    }
    
    function initMap(vJson,vDiv,vCuerpo) {
      vDual = false;
      if (vCuerpo != "") {
        vCuerpo = "#"+vCuerpo;
      }

      if (vJson['error'] != "") {
        vHtml = '<div class="alert alert-block alert-danger fade in">'+
                        '<button data-dismiss="alert" class="close close-sm" type="button">'+
                            '<i class="fa fa-times"></i>'+
                        '</button>'+
                        '<strong>Error</strong> <p>'+vJson['error']+'</p>'+
                    '</div>';
         $("#"+vDiv+" "+vCuerpo).html(vHtml);
         return true;
      }

      $("#"+vDiv+" "+vCuerpo).html('');

      var Elemento = $("#"+vDiv+" "+vCuerpo)[0];

      if (poli_destino['lat'] === undefined || poli_destino['lat'] === null) {
        if (poli['lat'] === undefined || poli['lat'] === null) {
          latitud_final = -33.359718;
          longitud_final = -70.705486;
          zoom_final = 8;
        }else{
          latitud_final = poli['lat'];
          longitud_final = poli['lng'];
          zoom_final = poli['zoom'];
        }
      }else{
        vDual = true;
        latitud_final = poli_destino['lat'];
        longitud_final = poli_destino['lng'];
        zoom_final = 10;
      }


      var haightAshbury = {lat: latitud_final, lng: longitud_final};
      map = new google.maps.Map(Elemento, {
        scrollwheel: false,
        zoom: zoom_final,
        center: haightAshbury,
        mapTypeId: google.maps.MapTypeId.SATELLITE 
      });
    
      var flightPath = new google.maps.Polyline({
        path: geopoli,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
      });

      flightPath.setMap(map);

      if (vDual == true) {
        var flightPath2 = new google.maps.Polyline({
          path: geopoli_destino,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });
  
        flightPath2.setMap(map);
      }

    
      // Adds a marker at the center of the map.
       $.each(vJson['datos'], function(index, val) {
          StrConductor = '';
          StrCelular = '';
          
           if (parseInt(val['longitud']) != 0 && parseInt(val['latitud']) != 0) {
              if (val['nombre_conductor'] == undefined) {
                var conductor = '';
                var celular = '';
                var registro = 'Sin registro de ingreso por sistema de tarjeta'; 
                if (val['empresa'] == undefined) {
                  var empresa = '';
                }else{
                  var empresa = val['empresa'];
                }
              }else{
                var conductor = val['nombre_conductor'];
                var celular = '+569'+val['celular_conductor'];
                var registro = '';
               if (val['empresa'] == undefined) {
                  var empresa = '';
                }else{
                  var empresa = val['empresa'];
                }
              }

              if (conductor != '') {
               StrConductor = '<li>Conductor: '+ conductor +'</li>';
              }

              if (celular != '') {
               StrCelular = '<li>Telefono: '+ celular +'</li>';
              }

              contentString = '<div id="tootip">'+
                '<div id="tooltiphead" class"row">'+
                '</div>'+
                '<h4>Patente: ' + val['patente'] + '</h4>'+
                '<div id="tooltipbody" class"row">'+
                '<ul>'+ 
                '<li>'+ registro +'</li>'+
                '<li>Empresa: '+ empresa +'</li>'+
                StrConductor +
                StrCelular +
                '</ul>'+
                '</div>'+
                '</div>';

              addMarker({lat: parseFloat(val['latitud']), lng: parseFloat(val['longitud'])},val['patente']);
           }
       });
    }
    
    // Adds a marker to the map and push to the array.
    function addMarker(location,vtitle) {
      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      infowindows.push(infowindow);

      var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: vtitle
      });
      markers.push(marker);

      google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
      });

      marker.addListener('click', function() {
        $.each(infowindows, function(index, val) {
           val.close();
        });
        infowindow.open(map, marker);
      });

    }
    
    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }
    
    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }
    
    // Shows any markers currently in the array.
    function showMarkers() {
      setMapOnAll(map);
    }
    
    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
      clearMarkers();
      markers = [];
    }