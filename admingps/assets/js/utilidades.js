/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function inputsUtilities(){
	var Today = new Date();
	var Day = (Today.getDate() < 10) ? "0"+Today.getDate() : Today.getDate();
	var Month = ((Today.getMonth()+1) < 10) ? "0"+(Today.getMonth() + 1) : (Today.getMonth() + 1) ;
	var Year = Today.getFullYear();

    var mindate = new Date(Today.getFullYear(), Today.getMonth(), Today.getDate(), 0, 0, 0, 0);

    $('[data-toggle="tooltip"]').tooltip();

    $('.default-date-picker').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        language: "es",
        autoclose: true
    });
    $('.default-date-picker').bind('dblclick', function() { 
    	$(this).val(Year+"-"+Month+"-"+Day);
    });

    $('.dmy-date-picker').datepicker({
        format: "dd-mm-yyyy",
        todayBtn: "linked",
        language: "es",
        autoclose: true
    });

    $('.dmy-date-picker-now').datepicker({
        format: "dd-mm-yyyy",
        todayBtn: "linked",
        language: "es",
        autoclose: true,
        startDate: mindate
    });

    $('.now-dmy').val(Day+"-"+Month+"-"+Year);
    
    $(".year-date-picker").datepicker( { 
        format: " yyyy", // Notice the Extra space at the beginning
        viewMode: "years", 
        minViewMode: "years"
    });
    $('.year-date-picker').bind('dblclick', function() { $(this).val(Year) });

    $('.spinnermonto').spinner({value:0, min: -99999999, max: 99999999});
    $('.spinner').spinner({value:0, min: -500000, max: 500000});
    $('.spinnerporcentaje').spinner({value:0, min: -1000, max: 1000}); 
    
    $('.inputSwitch').bootstrapSwitch('destroy');
    $('.inputSwitch').bootstrapSwitch();

    $('.multi-select').multiSelect();
}

function solo_numeros(event){
    if(event.shiftKey){
        event.preventDefault();
    }

    if (event.keyCode == 46 || event.keyCode == 8)    {
    }else{
        if (event.keyCode < 95) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }else{
            if (event.keyCode < 96 || event.keyCode > 105) {
                event.preventDefault();
            }
        }
    }
}

function ocultar(vId){
    $("#"+vId).hide(1000);;
}

function fechaNow(){
    var Today = new Date();
    var Day = (Today.getDate() < 10) ? "0"+Today.getDate() : Today.getDate();
    var Month = ((Today.getMonth()+1) < 10) ? "0"+(Today.getMonth() + 1) : (Today.getMonth() + 1) ;
    var Year = Today.getFullYear();
    var Hour = (Today.getHours() < 10) ? "0"+Today.getHours() : Today.getHours();
    var Min = (Today.getMinutes() < 10) ? "0"+Today.getMinutes() : Today.getMinutes();
    var Seg = (Today.getSeconds() < 10) ? "0"+Today.getSeconds() : Today.getSeconds();

    return Year+'-'+Month+'-'+Day+' '+Hour+':'+Min+':'+Seg;
}

function fechachile(date){
    var arr = date.split("-");

    return arr[2]+'-'+arr[1]+'-'+arr[0];
}

function construirModal(vUrl,obj,vTable,vModal){
    var headArray = [];
    var dataObj = {};
    Stopinterval(vModal);
    Stopinterval(vModal+'Aux');

    if (vTable != '') {
         $("#"+vTable+" thead tr").children("th.column").each(function(i) {
             headArray[i] = this.id.trim();
         });
         $(obj).children('.data').each(function(i) {
             dataObj[headArray[i]] = this.innerHTML.trim();
         });
    }else{
        dataObj = obj;
    }

    if($.browser.version > 8 && $.browser.version <= 11 && $.browser.mozilla)
    {
        var myDiv = $('#'+vModal+'Aux');

        if (myDiv.length){
            vModal += 'Aux';
        }        
    }

    $.ajax({
        url: vUrl,
        data: dataObj,
        type: 'POST',
        success: function (resp) {
            $("#"+vModal+" #cuerpomodal").html("");
            $("#"+vModal+" #cuerpomodal").html(resp);
            $("#"+vModal+"").modal("show");
            if($.browser.version > 8 && $.browser.msie)
            {$("#"+vModal+"").removeClass('fade');}
        },
        error: function(re) {
            console.log(re);
        }
    });
}   

function construirBasicModal(vBase,vUrl,vModal,dataM){
    Stopinterval(vModal);
    Stopinterval(vModal+'Aux');

    if($.browser.version > 8 && $.browser.version <= 11 && $.browser.mozilla)
    {
        var myDiv = $('#'+vModal+'Aux');

        if (myDiv.length){
            vModal += 'Aux';
        }        
    }

    $.ajax({
        url: vBase+vUrl,
        data: dataM,
        type: 'POST',
        beforeSend: function () {
            $("#"+vModal+" #cuerpomodal").html(vBase+'assets/images/input-spinner.gif">');
        },
        success: function (resp) {
            $("#"+vModal+" #cuerpomodal").html("");
            $("#"+vModal+" #cuerpomodal").html(resp);
            $("#"+vModal+"").modal("show");
            if($.browser.version > 8 && $.browser.msie)
            {$("#"+vModal+"").removeClass('fade');}
        },
        error: function(re) {
            console.log(re);
        }
    });
}   

