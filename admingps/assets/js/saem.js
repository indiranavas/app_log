function fechor_fmt(cfecha){
    var dateString =  new Date(cfecha);
    var e = new Date(cfecha);
	/* alert(e); */
    var fechor,dia,mes,ano
    dia=(e.getDate()+1);
    mes=(e.getMonth() +1);
    ano=e.getFullYear();
    fechora=(("0"+dia).slice(-2) + "/" + ("0"+mes).slice(-2) + "/" + ano);
    return fechora;
    }

function fechor_fmtlargo(cfecha){
    var dateString =  new Date(cfecha);
    var e =new Date(cfecha);
    var fechor,dia,mes,ano,hh,mm,ss
    dia=e.getDate();
    mes=(e.getMonth() +1);
    ano=e.getFullYear();
    hh=e.getHours();
    mm=e.getMinutes();
    ss=e.getSeconds();
    fechora=(("0"+dia).slice(-2) + "/" + ("0"+mes).slice(-2) + "/" + ano+' '+("0"+hh).slice(-2)+':'+("0"+mm).slice(-2)+':'+("0"+ss).slice(-2));
    return fechora;
    }	
	
function formatoNumero(numero, decimales, separadorDecimal, separadorMiles) {
    var partes, array;

    if ( !isFinite(numero) || isNaN(numero = parseFloat(numero)) ) {
        return "";
    }
    if (typeof separadorDecimal==="undefined") {
        separadorDecimal = ",";
    }
    if (typeof separadorMiles==="undefined") {
        separadorMiles = "";
    }

    // Redondeamos
    if ( !isNaN(parseInt(decimales)) ) {
        if (decimales >= 0) {
            numero = numero.toFixed(decimales);
        } else {
            numero = (
                Math.round(numero / Math.pow(10, Math.abs(decimales))) * Math.pow(10, Math.abs(decimales))
            ).toFixed();
        }
    } else {
        numero = numero.toString();
    }

    // Damos formato
    partes = numero.split(".", 2);
    array = partes[0].split("");
    for (var i=array.length-3; i>0 && array[i-1]!=="-"; i-=3) {
        array.splice(i, 0, separadorMiles);
    }
    numero = array.join("");

    if (partes.length>1) {
        numero += separadorDecimal + partes[1];
    }

    return numero;
}	

 