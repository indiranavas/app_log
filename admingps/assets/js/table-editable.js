var EditableTable = function () {

    return {
        //main function to initiate the module
        init: function (varT) {
            if(varT === undefined || varT === null || varT.length == 0)
                { varR = '.table-editable'; }
            else
                { varR = varT;}

            var oTable = $(varR).dataTable({
                "aLengthMenu": [
                    [10, 20, 30, -1],
                    [10, 20, 30, "Todo"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 10,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ Registros por Página",
                    "oPaginate": {
                        "sPrevious": "Ant.",
                        "sNext": "Sig."
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                "bPaginate" : true,
                "bFilter" : true
            });

            jQuery(varR+'_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery(varR+'_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            
        }

    };

}();