
var DashInterval;
var DashFuncion;
var DashValue;


function Startinterval(vFuncion,vValue){
    DashFuncion = vFuncion;
    DashValue = vValue;
    DashInterval = setInterval(vFuncion,vValue);
}

function Stopinterval(vModal){
    clearInterval(DashInterval);

    $("#"+vModal+"").on('hidden.bs.modal', function () {
    	Startinterval(DashFuncion,DashValue);
    });
}