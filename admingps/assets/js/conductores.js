function ajaxShowConductor(vUrl,vTab,vPatente,vModal){
    if($.browser.version > 8 && $.browser.version <= 11 && $.browser.mozilla)
    {
        var myDiv = $('#'+vModal+'Aux');

        if (myDiv.length){
            vModal += 'Aux';
        }        
    }
    Stopinterval(vModal);
    $.ajax({
        url: vUrl+'general/getconductor',
        type: 'POST',
        data: {tabla: vTab, patente: vPatente},
        dataType: 'json',
        beforeSend: function () {
            $("#"+vModal).modal("show");
            $("#"+vModal).on('shown.bs.modal', function () { 
                $("#"+vModal+" #cuerpomodal").html('<img src="'+vUrl+'assets/images/input-spinner.gif">');
            });
        },
        success: function (resp) {
            $("#"+vModal).on('shown.bs.modal', function () { 
                $("#"+vModal+" #cuerpomodal").html('<p>Patente: <b>'+resp['patente']+'</b> </p>'+
                                                    '<p>Empresa: '+resp['empresa']+'</p>'+
                                                    '<p>Conductor: '+resp['nombre_conductor']+'</p>'+
                                                    '<p>Teléfono: '+resp['celular_conductor']+'</p>'
                                                    );
            });
        }
    });  
}

function ajaxShowConductorTarjeta(vUrl,vTab,vPatente,vModal){
    if($.browser.version > 8 && $.browser.version <= 11 && $.browser.mozilla)
    {
        var myDiv = $('#'+vModal+'Aux');

        if (myDiv.length){
            vModal += 'Aux';
        }        
    }
    
    $.ajax({
        url: vUrl+'general/getconductortarjeta',
        type: 'POST',
        data: {tabla: vTab, patente: vPatente},
        dataType: 'json',
        beforeSend: function () {
            $("#"+vModal).modal("show");
            $("#"+vModal).on('shown.bs.modal', function () { 
                $("#"+vModal+" #cuerpomodal").html('<img src="'+vUrl+'assets/images/input-spinner.gif">');
            });
        },
        success: function (resp) {
            $("#"+vModal).on('shown.bs.modal', function () { 
                $("#"+vModal+" #cuerpomodal").html('<p>Patente: <b>'+resp['patente']+'</b> </p>'+
                                                    '<p>Empresa: '+resp['empresa']+'</p>'+
                                                    '<p>Conductor: '+resp['nombre_conductor']+'</p>'+
                                                    '<p>Teléfono: '+resp['celular_conductor']+'</p>'
                                                    );
            });
        }
    });  
}