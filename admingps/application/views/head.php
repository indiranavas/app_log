<!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="UTF-8">
		<title>Log Solutions</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/4.3.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />

		<!--[if IE]>
		<script src="assets/js/jquery.1.11.1.min.js"></script>
		<![endif]-->
		<script src="<?php echo base_url(); ?>assets/js/jquery.2.1.1.min.js"></script>		
		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>
		

		<!-- FULLCALENDAR -->
		<script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/scheduler.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/gcal.js"></script>
		<script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/lang-all.js"></script>
		<link href="<?php echo base_url();?>assets/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/css/plugins/fullcalendar/scheduler.min.css" rel="stylesheet">

				<?php echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');?>

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!-- ace settings handler -->
		<script src="<?php echo base_url(); ?>assets/js/ace-extra.min.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!-- basic scripts -->
		<!--[if !IE]> -->
		

		<!-- <![endif]-->

		
		<!-- <![endif]-->

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="<?php echo base_url(); ?>assets/js/jquery-ui.custom.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.easypiechart.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.flot.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.flot.pie.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.flot.resize.min.js"></script>
		<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.alerts.js"></script> -->

		<script src="<?php echo base_url(); ?>assets/js/bootbox.js"></script>

		<!-- ace scripts -->
		<script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/wizard.min.js"></script>

		<!-- highcharts scripts -->
		
		<script src="<?php echo base_url();?>assets/js/plugins/highcharts/highcharts.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/highcharts/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/heatmap.js"></script>
		<script src="https://code.highcharts.com/modules/tilemap.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>
		<script src="https://code.highcharts.com/modules/accessibility.js"></script>
		<!-- dashboard styles -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dashboard.css"/>

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/carrucel.css" />

		<script type='text/javascript'>
	        $(window).load(function() { // makes sure the whole site is loaded
	            $('#jpreLoader').fadeOut(); // will first fade out the loading animation
	            $('#jpreOverlay').fadeOut('normal'); // will fade out the white DIV that covers the website.
	            $('#main-container').css({'display':'block'});//  Revisar si esta asociado el correcto.
	        });
    	</script>
</head>
