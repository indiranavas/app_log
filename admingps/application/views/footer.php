		</div><!-- /.page-content -->
	</div><!-- /.main-content-inner -->
		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
			<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</a>
	</div><!-- /.main-content -->
</div><!-- /.main-container -->


<!-- BARRAS DE PROGRESO -->
<!-- define color de fondo de las barras de porcentaje y globos de porcentaje en las tablas-->
<script>
$(".progress-bar, .percentage-top-10").addClass(function(){
    var percentage = parseInt($(this).data('percent'), 10);
    if (percentage >= 0 && percentage < 50) {
        return 'progress-bar-49';
    }
    else if (percentage > 49 && percentage < 60) {
        return 'progress-bar-59';
    }
    else if (percentage > 59 && percentage <= 100) {
        return 'progress-bar-100';
    }    
});
</script>

<!-- PORCENTAJE CIRCULAR CON COLOR-->
<!-- define color del numero del porcentaje circular -->
<script>
$(".easy-pie-chart.percentage.percentage-general, .header-pie-progress").addClass(function(){
    var percentage = parseInt($(this).data('percent'), 10);
    if (percentage > 0 && percentage < 50){
        return 'progress-bar-49-percent-number'
    }
    else if (percentage > 49 && percentage < 60) {
        return 'progress-bar-59-percent-number';
    }
    else if (percentage > 59 && percentage <= 100) {
        return 'progress-bar-100-percent-number';
    }    
});
</script>

<!-- pie percentage-general -->
<script>
	jQuery(function($) {
		$('.easy-pie-chart.percentage.percentage-general').each(function(){
			var percentage = parseInt($(this).data('percent'), 10);
			var $box = $(this).closest('.infobox');
			var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
			var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
			var size = parseInt($(this).data('size')) || 100;

			if (percentage >= 0 && percentage < 50){
	        	$(this).easyPieChart({
					barColor: '#fa6464',
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
					size: size
				});
    		}
    		if (percentage > 49 && percentage < 60){
	        	$(this).easyPieChart({
					barColor: '#fab264',
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
					size: size
				});
    		}
    		if (percentage > 59 && percentage <= 100){
	        	$(this).easyPieChart({
					barColor: '#93c020',
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
					size: size
				});
    		}

			
		})
		
	
	  //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
	  //but sometimes it brings up errors with normal resize event handlers
	  $.resize.throttleWindow = false;
	})
</script>

<!-- header-pie-progress -->

<script>
	jQuery(function($) {
		$('.easy-pie-chart.header-pie-progress').each(function(){
			var percentage = parseInt($(this).data('percent'), 10);
			var $box = $(this).closest('.infobox');
			var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
			var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
			var size = parseInt($(this).data('size')) || 52;

			if (percentage > 0 && percentage < 50){
	        	$(this).easyPieChart({
					barColor: '#fa6464',
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
					size: size
				});
    		}
    		if (percentage > 49 && percentage < 60){
	        	$(this).easyPieChart({
					barColor: '#fab264',
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
					size: size
				});
    		}
    		if (percentage > 59 && percentage <= 100){
	        	$(this).easyPieChart({
					barColor: '#93c020',
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
					size: size
				});
    		}

			
		})
		
	
	  //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
	  //but sometimes it brings up errors with normal resize event handlers
	  $.resize.throttleWindow = false;
	})
</script>
<script type="text/javascript">
    $('[name="cambiar"]').on('click',function(){
		//document.getElementById("loading").style.display = "inline";
		document.getElementById("jpreOverlay").style.display = "block";
		});


</script>
</body>
</html>
