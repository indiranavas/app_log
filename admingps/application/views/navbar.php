<body class="skin-3">
	<div id="jpreOverlay"><!-- poner este div justo despues del inicio del body -->
        <div id="jpreSlide">
            <i class="fa fa-refresh fa-spin" ></i>
            <p> Un momento por favor... </p>
        </div>
    </div>
	<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="<?php echo base_url(); ?>" className="navbar-brand">
						<img src="<?php echo base_url(); ?>assets/img/LOG.png" width="100">
						<img src="<?php echo base_url(); ?>assets/img/solutions.png" width="100">
					</a>
					
				</div>

				<div class="flota-izq" >
					<span class="color2 fuente" ><h4>TMS Optimo</h4></span>
				</div>
				

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-green1 dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-comment-o"></i>
								<!--<span class="badge badge-grey">3</span> -->
							</a>
						</li>

						<li class="light-green2 dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-tasks"></i>
								<!--<span class="badge badge-grey">3</span> -->
							</a>
						</li>

						<li class="light-green3 dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell icon-animated-bell"></i>
								<!--<span class="badge badge-grey">3</span> -->
							</a>
						</li>

						<li class="light-green4 dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-envelope icon-animated-vertical"></i>
								<!--<span class="badge badge-grey">3</span> -->
							</a>
						</li>

						<li class="light-black dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<!-- <img class="nav-user-photo" src="<?php //echo base_url(); ?>assets/images/avatars/avatar4.png" alt="Jason's Photo" /> -->
								<span class="user-info">
									<small>Bienvenido,</small>
									Administrador
									<?php echo $this->session->userdata('usuario') ; ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="">
										<i class="ace-icon fa fa-user"></i>
										Perfil
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="https://pp.log.solutions">
										<i class="ace-icon fa fa-power-off"></i>
										Cerrar Sesión
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>
	</div>
	<div class="main-container" id="main-container">
	<div id="loading"></div>
		
<script type="text/javascript">
	try{ace.settings.check('main-container' , 'fixed')}catch(e){}
</script>
