<div id="sidebar" class="sidebar responsive" >
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>
				<ul class="nav nav-list">					
					<li <?php if ($activar_menu== 'entrega') echo 'class="open"' ?> >
						<a href="#" class="dropdown-toggle bgcolor  text-info">
							<i class="menu-icon fa fa-briefcase text-info"></i>
							<span class="menu-text bgcolor text-info" > Control de entrega </span>

							<b class="arrow fa fa-angle-down text-info"></b>
						</a>
						<b class="arrow text-info"></b>

						<ul class="submenu bgcolor2">
							<li <?php if ($currentPage== 'dashboard') echo 'class="active"' ?>>
								<a href="<?php echo base_url(); ?>entrega/dashboard">
									<i class="menu-icon fa fa-caret-right"></i>
									Dashboard
								</a>

								<b class="arrow"></b>
							</li>
							<!----
							<li <?php //if ($currentPage== 'gestion_entrega') echo 'class="active"' ?>>
								<a href="<?php //echo base_url(); ?>entrega/gestion_entrega">
									<i class="menu-icon fa fa-caret-right"></i>
									Gestión de entrega
								</a>

								<b class="arrow"></b>
							</li>
							-->
							<li <?php if ($currentPage== 'carga_prog') echo 'class="active"' ?>>
								<a href="<?php echo base_url(); ?>Carga_prog">
									<i class="menu-icon fa fa-caret-right"></i>
									Carga de Programación
								</a>

								<b class="arrow"></b>
							</li>

							<li <?php if ($currentPage== 'historico_pod') echo 'class="active"' ?>>
								<a href="<?php echo base_url(); ?>entrega/historico_pod">
									<i class="menu-icon fa fa-caret-right"></i>
									Historico POD
								</a>

								<b class="arrow"></b>
							</li>
							<!----
							<li <?php // if ($currentPage== 'incidentes') echo 'class="active"' ?>>
								<a href="<?php // echo base_url(); ?>entrega/gestion_incidentes">
									<i class="menu-icon fa fa-caret-right"></i>
									Gestión de incidentes
								</a>

								<b class="arrow"></b>
							</li>
							-->
						</ul>
					</li>	
						
					
				</ul><!-- /.nav-list -->
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>

