<p class="bg-success center">Carga de Programación</p>

<div class="row">
	<div class="col-sm-10">
	
	<div class="widget-body">
		<div class="widget-main">
		
		<div id="fuelux-wizard-container">
			<div>
				<ul class="steps">
					<li data-step="1" class="active">
						<span class="step">1</span>
						<span class="title">Preparación</span>
					</li>

					<li data-step="2">
						<span class="step">2</span>
						<span class="title">Coordinación</span>
					</li>

					<li data-step="3">
						<span class="step">3</span>
						<span class="title">Asignación</span>
					</li>
					
					<li data-step="4">
						<span class="step">4</span>
						<span class="title">Programación</span>
					</li>

					<li data-step="5">
						<span class="step">5</span>
						<span class="title">Ajuste a Pedidos</span>
					</li>
				</ul>
			</div>	

			<hr />	
			

			<div class="step-content pos-rel">
				<div class="step-pane active" data-step="1">
					<div data-toggle="buttons" class="btn-group">
						<button class="btn btn-app-md btn-yellow btn-xs">
						<h6>Cargar Archivo
						(Excel cvs)</h6>
						</button>
						<button class="btn btn-app-md btn-danger btn-xs">
						<h6>Integración <br> Automática</h6>
						</button>
						<button class="btn btn-app-md btn-primary btn-xs">
						<h6>Cargar <br> Manual</h6>
						</button>
					</div>
					<address>
						<strong><a href="mailto:#">Formato Archivo</a></strong>
					</address>
					
					<!-- tabla -->
					<table class="table table-bordered table-responsive">
  						<tr class="bg-blue">
							<th>Id Pedido</th>
							<th>Fecha Pedido</th>
							<th>Pedidos Cargados</th>
							<th>OC</th>
							<th>Cliente</th>
							<th>Fecha Estimada Entrega</th>
							<th>Tipo de Carga</th>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						<tr>
							<td>1</td>
							<td>10-10-2020</td>
							<td>Pedido 1</td>
							<td>203782</td>
							<td>Totus</td>
							<td>20-10-2020</td>
							<td>Manual</td>
						</tr>
						
					</table>
				</div>

				<div class="step-pane" data-step="2">
					<p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
				</div>

				<div class="step-pane" data-step="3">
					<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
				</div>

				<div class="step-pane" data-step="4">
					<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin.</p>
				</div>
				
				<div class="step-pane" data-step="5">
					<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin.</p>
				</div>
			</div>
		</div>
		<hr />
		<div class="wizard-actions">
			<button class="btn btn-prev">
				<i class="ace-icon fa fa-arrow-left"></i>
				Anterior
			</button>

			<button class="btn btn-success btn-next" data-last="Final">
				Siguiente
				<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
			</button>
		</div>
	
	
	</div>
	</div>
	</div><!-- /.col -->
</div>	

<!-- inline scripts related to this page -->
<script type="text/javascript">
jQuery(function($) {


	var $validation = false;
	$('#fuelux-wizard-container')
	.ace_wizard({
		//step: 2 //optional argument. wizard will jump to step "2" at first
		//buttons: '.wizard-actions:eq(0)'
	})
	/* .on('actionclicked.fu.wizard' , function(e, info){
		if(info.step == 1 && $validation) {
			if(!$('#validation-form').valid()) e.preventDefault();
		}
	}) */
	/* .on('changed.fu.wizard', function() {
	 
	}) */
	.on('finished.fu.wizard', function(e) {
		bootbox.dialog({
			message: "Thank you! Your information was successfully saved!", 
			buttons: {
				"success" : {
					"label" : "OK",
					"className" : "btn-sm btn-primary"
				}
			}
		});
	}).on('stepclick.fu.wizard', function(e){
		//e.preventDefault();//this will prevent clicking and selecting steps
	});
})
</script>
