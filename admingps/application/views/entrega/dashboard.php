
<!-- inicio contenidos -->

<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>
            
            <ul class="breadcrumb">
                <li class="active"><a href="<?php echo base_url(); ?>">Cliente: LOG SOLUTIONS SPA</a></li>
            <li class="active"></i><a href="<?php echo base_url(); ?>">Subcliente: LOG SOLUTIONS</a></li>
        <li class="active"></i><a href="<?php echo base_url(); ?>">Pais: CHILE</a></li>
        <li ><a href="<?php echo base_url(); ?>/entrega">Dashboard</a></li>
    </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
    <div id="carrucel"></div>
    <div class="row ">
        <section class="panel " style="overflow: auto;">
        
            <div class="col-sm-12  marco">
            <div class="panel panel-default">
                <h6 class="text-info center"><label id="et-traslado"></label>
                <button type="button" onclick="es_mensual();" class="btn btn-warning btn-xs active">Mensual</button>
                <button type="button" onclick="es_trimes();" class="btn btn-primary btn-xs">Trimestral</button>
                <button type="button" onclick="es_anual();" class="btn btn-success btn-xs">Anual</button>
                </h6>
                <div class="panel-body center">

                <div class="col-sm-4 ">
                    <table widht="100%" >
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                           
                            <td>
                                <div id="container" class='dashboard-achique zoom' style="min-width: 325px; height: 125px; margin: 0 auto"></div>
                            </td>
                        </tr>
                        <tr>
                             <td>                    
                                <div class= 'text-center'>
                                    <h6>
                                    <span class="infobox-green2"><label id="et-t1"></label></span> 
                                    % en Tiempo
                                    <div id="et-t2c" class="badge badge-success">
                                       <label id="et-t2"></label>
                                        <i class="ace-icon fa fa-arrow-down"></i>
                                    </div>
                                    </h6>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                </div>
                <div class="col-sm-4">
                    <table widht="100%">
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <div id="container2" class='dashboard-achique zoom' style="min-width: 325px; height: 125px; margin: 0 auto"></div>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <div class= 'text-center'>
                                    <h6>
                                    <span class="infobox-blue2"><label id="et-t3"></label></span> 
                                    Km. | Traslados
                                    <div id="et-t4c" class="badge badge-success">
                                       <label id="et-t4"></label>
                                        <i class="ace-icon fa fa-arrow-up"></i>
                                    </div>
                                    </h6>
                                </div>
                               </td> 
                        </tr>
                        </table>
                    </div>
                    <div class="col-sm-4">
                        <table widht="100%">
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                
                                    <td>
                                        <div id="container3" class='dashboard-achique zoom' style="min-width: 325px; height: 125px; margin: 0 auto"></div>
                                    </td>
                                </tr>
                             <tr>
                             <td>
                                    <div class= 'text-center'>
                                        <h6>
                                        <span class="infobox-brown2"><label id="et-t5"></label></span> 
                                        % Finalizadas
                                        <div id="et-t6c" class="badge badge-success">
                                            <label id="et-t6"></label>
                                            <i class="ace-icon fa fa-arrow-down"></i>
                                        </div>
                                        </h6>
                                    </div>
                                   </td> 
                             </tr>
                            </table>
                        </div>
                </div>  
              </div>  
            </div>
                    
                    
                    
                    <div class="col-sm-12 groove-dashboard2">
                    
                        <div class="col-sm-4 marco">
                     <div class="panel panel-default">   
                        <h6 class="text-info center">Estado</h6>
                        <div class="panel-body center">
                                    <div id="container4" class='dashboard-achique zoom' style="min-width: 320px; height: 200px; margin: 0 auto"></div>
                        </div>
                        </div>     
                     </div>   
                     
                        <div class="col-sm-8 marco">
                     <div class="panel panel-default">      
                        <h6 class="text-info center">Número de Estados por Años</h6>
                        <div class="panel-body center">
                            <div id="container5" class='dashboard-achique zoom' style="min-width: 660px; height: 200px; margin: 0 auto"></div>
                        </div>    
                        </div> 
                    </div>  
                    </div>
                    
                    <div class="col-sm-12  groove-dashboard2">
                    
                        <div class="col-sm-4 marco">
                       <div class="panel panel-default"> 
                        <h6 class="text-info center">Volumen de Entregas</h6>
                        <div class="panel-body center">
                            <div id="container6" class='dashboard-achique zoom' style="min-width: 325px; height: 200px; margin: 0 auto"></div>
                        </div>    
                        </div>
                        </div>
                     
                        <div class="col-sm-4 marco">
                       <div class="panel panel-default">    
                        <h6 class="text-info center">Zonas</h6>
                        <div class="panel-body center">
                             <div id="container7"  class='dashboard-achique zoom' style="min-width: 325px; height: 200px; margin: 0 auto"></div>
                        </div>
                        </div>
                        </div>
                        
                        <div class="col-sm-4 marco"> 
                        <div class="panel panel-default">   
                        <h6 class="text-info center">Total de Entregas por Meses</h6>
                        <div class="panel-body center">
                            <div id="container8" class='dashboard-achique zoom' style="min-width: 325px; height: 200px; margin: 0 auto"></div>
                        </div>
                        </div>
                        </div>
                        
                    </div>
                    
                    
                    
                </section>
            </div>
        </div><!-- /.row  -->
        
        <style>
                    * {
            box-sizing: border-box;
        }
        .zoom {
            transition: transform .2s; Animation
            margin: 0 auto; 
        }

        .zoom:hover {
            transform: scale(1.3); 
            z-index: 999; /* Sit on top */
             position:relative;
        }
        </style>
        
        
        <script>
        tras_activo=sessionStorage.getItem('tipoTraslado');
        /* alert(tras_activo); */
        if (tras_activo == null) {
            var tras_activo='Mensual';
           sessionStorage.setItem('tipoTraslado','Mensual');
            /* alert(tras_activo); */
        } else {
            tras_activo=sessionStorage.getItem('tipoTraslado');
            /* alert(tras_activo); */
        }
        
        
        if (tras_activo=='Mensual' ){
        document.getElementById('et-traslado').innerHTML= 'Traslados Mensual';
        document.getElementById('et-t1').innerHTML= '25';
        document.getElementById('et-t2').innerHTML= '-12';
        document.getElementById('et-t2c').style.backgroundColor =  'red';
        document.getElementById('et-t3').innerHTML= '2.115';
        document.getElementById('et-t4').innerHTML= '21';
        document.getElementById('et-t4c').style.backgroundColor =  'green';
        document.getElementById('et-t5').innerHTML= '34';
        document.getElementById('et-t6').innerHTML= '-3';
        document.getElementById('et-t6c').style.backgroundColor =  'red';
            var tras_activo='Mensual';
        }    
        
         if (tras_activo=='Trimestral'){
        document.getElementById('et-traslado').innerHTML= 'Traslados Trimestral';
        document.getElementById('et-t1').innerHTML= '60';
        document.getElementById('et-t2').innerHTML= '25';
        document.getElementById('et-t2c').style.backgroundColor =  'green';
        document.getElementById('et-t3').innerHTML= '3.115';
        document.getElementById('et-t4').innerHTML= '40';
        document.getElementById('et-t4c').style.backgroundColor =  'green';
        document.getElementById('et-t5').innerHTML= '30';
        document.getElementById('et-t6').innerHTML= '-5';
        document.getElementById('et-t6c').style.backgroundColor =  'red';
            var tras_activo='Trimestral';
        }    
        
         if (tras_activo=='Anual'){
        document.getElementById('et-traslado').innerHTML= 'Traslados Anual';
        document.getElementById('et-t1').innerHTML= '160';
        document.getElementById('et-t2').innerHTML= '-18';
        document.getElementById('et-t2c').style.backgroundColor =  'red';
        document.getElementById('et-t3').innerHTML= '5.115';
        document.getElementById('et-t4').innerHTML= '45';
        document.getElementById('et-t4c').style.backgroundColor =  'green';
        document.getElementById('et-t5').innerHTML= '130';
        document.getElementById('et-t6').innerHTML= '15';
        document.getElementById('et-t6c').style.backgroundColor =  'green';
            var tras_activo='Anual';
        }    
            /* sessionStorage.tipoTraslado=tras_activo; */
        
            $(document).ready(function(e) 
            {
                carrucel();
                
                
            });
            
            function es_mensual(){
                sessionStorage.setItem('tipoTraslado','Mensual');
                document.getElementById('et-traslado').innerHTML= 'Traslados Mensual';
                location.reload();
                }
                
            function es_trimes(){
                sessionStorage.setItem('tipoTraslado','Trimestral');
                document.getElementById('et-traslado').innerHTML= 'Traslados Trimestral';
                location.reload();
                }    
            
            function es_anual(){
                sessionStorage.setItem('tipoTraslado','Anual');
                document.getElementById('et-traslado').innerHTML= 'Traslados Anual';
                location.reload();
                }
            
            $(function () {
            tras_activo=sessionStorage.getItem('tipoTraslado');
            /* alert(tras_activo); */
            if (tras_activo=="Mensual"){
                $('#container').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                        categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
                    },
                    yAxis: {
                        title: {
                            text: 'Q. Traslados'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 0,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                        
                        [
                        0,
                        0.7537
                        ],

[
                        1,
                        1.5537
                        ],
                        
                        [
                        2,
                        0.9537
                        ],

                        [
                        3,
                        2.3537
                        ],
                        
                        [
                        4,
                        1.9537
                        ],

                        [
                        5,
                        2.1537
                        ],
                        
                        [
                        6,
                        0.7537
                        ],

                        [
                        7,
                        1.5537
                        ],
                        
                        [
                        8,
                        0.9537
                        ],

                        [
                        9,
                        2.3537
                        ],
                        
                        [
                        10,
                        1.9537
                        ],

                        [
                        11,
                        2.1537
                        ],

                        ]
                    }]
                });
            }
                
            if (tras_activo=="Trimestral"){
                $('#container').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                        categories: ['1er.', '2do.', '3ro.', '4to.']
                    },
                    yAxis: {
                        title: {
                            text: 'Q. Traslados'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 0,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                        
                        [
                        0,
                        0.7537
                        ],

                        [
                        1,
                        1.5537
                        ],
                        
                        [
                        2,
                        0.9537
                        ],

                        [
                        3,
                        2.3537
                        ],
                        
                        

                        ]
                    }]
                });   
            }    
                
             
            if (tras_activo=="Anual"){
                $('#container').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                        categories: ['2010', '2011', '2012', '2013', '2014', '2015',
                '2016', '2017', '2018', '2019', '2020']
                    },
                    yAxis: {
                        title: {
                            text: 'Q. Traslados'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 0,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                        
                        [
                        0,
                        0.7537
                        ],

                        [
                        1,
                        1.5537
                        ],
                        
                        [
                        2,
                        0.9537
                        ],

                        [
                        3,
                        2.3537
                        ],
                        
                        [
                        4,
                        1.9537
                        ],

                        [
                        5,
                        2.1537
                        ],
                        
                        [
                        6,
                        3.7537
                        ],

                        [
                        7,
                        3.5537
                        ],
                        
                        [
                        8,
                        4.9537
                        ],

                        [
                        9,
                        3.3537
                        ],
                        
                        [
                        10,
                        4.9547
                        ],


                        ]
                    }]
                });             
            } 
                
                
             if (tras_activo=="Mensual"){    
                $('#container2').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                         categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
                    },
                    yAxis: {
                        title: {
                            text: 'Km. Recorridos'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[3]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                       [
                        0,
                        2.7537
                        ],

                        [
                        1,
                        3.5537
                        ],
                        
                        [
                        2,
                        0.9537
                        ],

                        [
                        3,
                        2.3537
                        ],
                        
                        [
                        4,
                        2.9537
                        ],

                        [
                        5,
                        3.1537
                        ],
                        
                        [
                        6,
                        2.7537
                        ],

                        [
                        7,
                        4.5537
                        ],
                        
                        [
                        8,
                        5.9537
                        ],

                        [
                        9,
                        3.3537
                        ],
                        
                        [
                        10,
                        6.9537
                        ],

                        [
                        11,
                        5.1537
                        ],
                        ]
                    }]
                });
             }
             
             if (tras_activo=="Trimestral"){    
                $('#container2').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                          categories: ['1er.', '2do.', '3ro.', '4to.']
                    },
                    yAxis: {
                        title: {
                            text: 'Km. Recorridos'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[3]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                       [
                        0,
                        2.7537
                        ],

                        [
                        1,
                        3.5537
                        ],
                        
                        [
                        2,
                        0.9537
                        ],

                        [
                        3,
                        2.3537
                        ],
                        
                       
                        ]
                    }]
                });
             }     

             if (tras_activo=="Anual"){    
                $('#container2').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                         categories: ['2010', '2011', '2012', '2013', '2014', '2015',
                '2016', '2017', '2018', '2019', '2020']
                    },
                    yAxis: {
                        title: {
                            text: 'Km. Recorridos'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[3]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                       [
                        0,
                        2.7537
                        ],

                        [
                        1,
                        3.5537
                        ],
                        
                        [
                        2,
                        0.9537
                        ],

                        [
                        3,
                        2.3537
                        ],
                        
                        [
                        4,
                        2.9537
                        ],

                        [
                        5,
                        3.1537
                        ],
                        
                        [
                        6,
                        2.7537
                        ],

                        [
                        7,
                        4.5537
                        ],
                        
                        [
                        8,
                        5.9537
                        ],

                        [
                        9,
                        3.3537
                        ],
                        
                        [
                        10,
                        6.9537
                        ],

                      
                        ]
                    }]
                });
             }
                
             if (tras_activo=="Mensual"){                    
                $('#container3').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                         categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
                    },
                    yAxis: {
                        title: {
                            text: 'Q. Entregas'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[2]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 0,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                       [
                        0,
                        4.7537
                        ],

                        [
                        1,
                        3.5537
                        ],
                        
                        [
                        2,
                        2.9537
                        ],

                        [
                        3,
                        3.3537
                        ],
                        
                        [
                        4,
                        4.9537
                        ],

                        [
                        5,
                        2.1537
                        ],
                        
                        [
                        6,
                        3.7537
                        ],

                        [
                        7,
                        4.5537
                        ],
                        
                        [
                        8,
                        6.9537
                        ],

                        [
                        9,
                        5.3537
                        ],
                        
                        [
                        10,
                        8.9537
                        ],

                        [
                        11,
                        6.1537
                        ],
                        ]
                    }]
                });
             }
             
             if (tras_activo=="Trimestral"){                    
                $('#container3').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                          categories: ['1er.', '2do.', '3ro.', '4to.']
                    },
                    yAxis: {
                        title: {
                            text: 'Q. Entregas'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[2]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 0,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                       [
                        0,
                        4.7537
                        ],

                        [
                        1,
                        3.5537
                        ],
                        
                        [
                        2,
                        2.9537
                        ],

                        [
                        3,
                        3.3537
                        ],
                        
                       
                        ]
                    }]
                });
             }             
                
                
             if (tras_activo=="Anual"){                    
                $('#container3').highcharts({
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: 
                        ''
                    },
                    xAxis: {
                         categories: ['2010', '2011', '2012', '2013', '2014', '2015',
                '2016', '2017', '2018', '2019', '2020']
                    },
                    yAxis: {
                        title: {
                            text: 'Q. Entregas'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                [0, Highcharts.getOptions().colors[2]],
                                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 0,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    
                    series: [{
                        type: 'area',
                        name: 'Meses',
                        data: [
                       [
                        0,
                        4.7537
                        ],

                        [
                        1,
                        3.5537
                        ],
                        
                        [
                        2,
                        2.9537
                        ],

                        [
                        3,
                        3.3537
                        ],
                        
                        [
                        4,
                        4.9537
                        ],

                        [
                        5,
                        2.1537
                        ],
                        
                        [
                        6,
                        3.7537
                        ],

                        [
                        7,
                        4.5537
                        ],
                        
                        [
                        8,
                        6.9537
                        ],

                        [
                        9,
                        5.3537
                        ],
                        
                        [
                        10,
                        8.9537
                        ],

                        ]
                    }]
                });
             }
             
             
                $('#container4').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },
                    title: {
                        text: 'Eventos<br>de Entregas<br>2020',
                        align: 'center',
                        verticalAlign: 'middle',
                        y:20,
                        x:-10
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -20,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: ['45%', '85%'],
                            size: '200%'
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Estado de Entregas',
                        innerSize: '60%',
                        data: [
                        ['Éxito', 38.9],
                        ['Falló', 18.29],
                        ['Retirado', 23],
                        ['Sin Retirar', 8.78],
                        ['Devuelto', 8.42],
                        {
                            name: 'En Transito',
                            y: 12.61,
                            dataLabels: {
                                enabled: true
                            }
                        }
                        ]
                    }]
                });
                
                
                $('#container5').highcharts({
                    chart: {
                        type: 'area'
                    },
                    accessibility: {
                        description: 'Image description: An area chart compares the nuclear stockpiles of the USA and the USSR/Russia between 1945 and 2017. The number of nuclear weapons is plotted on the Y-axis and the years on the X-axis. The chart is interactive, and the year-on-year stockpile levels can be traced for each country. The US has a stockpile of 6 nuclear weapons at the dawn of the nuclear age in 1945. This number has gradually increased to 369 by 1950 when the USSR enters the arms race with 6 weapons. At this point, the US starts to rapidly build its stockpile culminating in 32,040 warheads by 1966 compared to the USSR’s 7,089. From this peak in 1966, the US stockpile gradually decreases as the USSR’s stockpile expands. By 1978 the USSR has closed the nuclear gap at 25,393. The USSR stockpile continues to grow until it reaches a peak of 45,000 in 1986 compared to the US arsenal of 24,401. From 1986, the nuclear stockpiles of both countries start to fall. By 2000, the numbers have fallen to 10,577 and 21,000 for the US and Russia, respectively. The decreases continue until 2017 at which point the US holds 4,018 weapons compared to Russia’s 4,500.'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        allowDecimals: false,
                        labels: {
                            formatter: function () {
                                return this.value; // clean, unformatted number for year
                            }
                        },
                        accessibility: {
                            rangeDescription: 'Rango: 2010 to 2020.'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Números de Estados'
                        },
                        labels: {
                            formatter: function () {
                                return this.value / 1 + ' N';
                            }
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name} realizadas/os <b>{point.y:,.0f}</b><br/>hasta en {point.x}'
                    },
                    plotOptions: {
                        area: {
                            pointStart: 2010,
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Traslados',
                        data: [
                        456, 311, 532, 610, 735,
                        869, 940, 1005, 1436, 2063, 3057 
                        ]
                        }, {
                        name: 'Entregas',
                        data: [500, 425, 350, 420, 650, 1300, 1426, 1860, 2069, 3054,
                        1605 
                        ]
                        }, {
                        name: 'Alertas',
                        data: [300, 240, 400, 325, 280, 350, 626, 760, 969, 760,
                        1605
                        ]
                        }
                    ]
                });
                
                $('#container6').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: ['Centro', 'Occidente', 'Andes', 'Zulia', 'Oriente'],
                        title: {
                            text: 'Regiones'
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Volumen (entregas)',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' Entregas'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 0,
                        floating: false,
                        borderWidth: 1,
                        backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Año 2010',
                        data: [107, 31, 635, 203,50]
                        }, {
                        name: 'Año 2014',
                        data: [133, 156, 947, 408,76]
                        }, {
                        name: 'Año 2018',
                        data: [814, 841, 2714, 727,231]
                        }, {
                        name: 'Año 2020',
                        data: [1216, 1001, 2436, 738, 340]
                    }]
                });
                
                $('#container7').highcharts({
                    chart: {
                        type: 'tilemap',
                        inverted: true,
                        height: '60%'
                    },
                    
                    accessibility: {
                        description: 'A tile map represents the states of the USA by population in 2016. The hexagonal tiles are positioned to geographically echo the map of the USA. A color-coded legend states the population levels as below 1 million (beige), 1 to 5 million (orange), 5 to 20 million (pink) and above 20 million (hot pink). The chart is interactive, and the individual state data points are displayed upon hovering. Three states have a population of above 20 million: California (39.3 million), Texas (27.9 million) and Florida (20.6 million). The northern US region from Massachusetts in the Northwest to Illinois in the Midwest contains the highest concentration of states with a population of 5 to 20 million people. The southern US region from South Carolina in the Southeast to New Mexico in the Southwest contains the highest concentration of states with a population of 1 to 5 million people. 6 states have a population of less than 1 million people; these include Alaska, Delaware, Wyoming, North Dakota, South Dakota and Vermont. The state with the lowest population is Wyoming in the Northwest with 584,153 people.',
                        screenReaderSection: {
                            beforeChartFormat:
                            '<h5>{chartTitle}</h5>' +
                            '<div>{chartSubtitle}</div>' +
                            '<div>{chartLongdesc}</div>' +
                            '<div>{viewTableButton}</div>'
                        },
                        point: {
                            valueDescriptionFormat: '{index}. {xDescription}, {point.value}.'
                        }
                    },
                    
                    title: {
                        text: ''
                    },
                    
                    subtitle: {
                        text: ''
                    },
                    
                    xAxis: {
                        visible: false
                    },
                    
                    yAxis: {
                        visible: false
                    },
                    
                    colorAxis: {
                        dataClasses: [{
                            from: 0,
                            to: 49999,
                            color: '#FFA500',
                            name: '< 10k'
                            }, {
                            from: 50000,
                            to: 199999,
                            color: '#4169Ee',
                            name: '50k - 100k'
                            }, {
                            from: 200000,
                            to: 499999,
                            color: '#FF7987',
                            name: '200k - 300k'
                            }, {
                            from: 500000,
                            color: '#FF4500',
                            name: '> 500k'
                        }]
                    },
                    
                    tooltip: {
                        headerFormat: '',
                        pointFormat: 'Entregas en <b> {point.name}</b> fue de <b>{point.value}</b>'
                    },
                    
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '{point.hc-a2}',
                                color: '#000000',
                                style: {
                                    textOutline: false
                                }
                            }
                        }
                    },
                    
                    series: [{
                        name: '',
                        data: [{
                            'hc-a2': 'CS',
                            name: 'Caracas',
                            region: 'center',
                            x: 0,
                            y: 5,
                            value: 584937
                            }, {
                            'hc-a2': 'BQ',
                            name: 'Barquisimeto',
                            region: 'West',
                            x: 3,
                            y: 3,
                            value: 73773
                            }, {
                            'hc-a2': 'VA',
                            name: 'Valencia',
                            region: 'center',
                            x: 2,
                            y: 3,
                            value: 674540
                            }, {
                            'hc-a2': 'ME',
                            name: 'Mérida',
                            region: 'South',
                            x: 4,
                            y: 2,
                            value: 299407
                            }, {
                            'hc-a2': 'CO',
                            name: 'Coro',
                            region: 'West',
                            x: 1,
                            y: 1,
                            value: 3925001
                            }, {
                            'hc-a2': 'BA',
                            name: 'Barinas',
                            region: 'South',
                            x: 5,
                            y: 1,
                            value: 554054
                            }, {
                            'hc-a2': 'SC',
                            name: 'San Cristobal',
                            region: 'South',
                            x: 6,
                            y: 0,
                            value: 359667
                            }, {
                            'hc-a2': 'TR',
                            name: 'Trujillo',
                            region: 'South',
                            x: 3,
                            y: 2,
                            value: 93561
                            }, {
                            'hc-a2': 'MY',
                            name: 'Maracay',
                            region: 'South',
                            x: 1,
                            y: 4,
                            value: 72880
                            }, {
                            'hc-a2': 'MA',
                            name: 'Maturin',
                            region: 'South',
                            x: 1,
                            y: 6,
                            value: 206129
                            }, {
                            'hc-a2': 'VI',
                            name: 'el Vigía',
                            region: 'South',
                            x: 5,
                            y: 2,
                            value: 103101
                            }, {
                            'hc-a2': 'MC',
                            name: 'Maracaibo',
                            region: 'West',
                            x: 1,
                            y: 0,
                            value: 14195
                            }, {
                            'hc-a2': 'MG',
                            name: 'Margarita',
                            region: 'West',
                            x: 0,
                            y: 6,
                            value: 163444
                            }, {
                            'hc-a2': 'PC',
                            name: 'Puerto La Cruz',
                            region: 'Midwest',
                            x: 1,
                            y: 7,
                            value: 128019
                            }]
                    }]
                });
                
                $('#container8').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Entregas (miles)'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Total entregas en 2020: <b>{point.y:.1f} Miles</b>'
                    },
                    series: [{
                        name: 'Entregas',
                        data: [
                        ['Enero', 24.2],
                        ['Febrero', 20.8],
                        ['Marzo', 14.9],
                        ['Abril', 23.7],
                        ['Mayo', 18.1],
                        ['Junio', 22.7],
                        ['Julio', 32.4],
                        ['Agosto', 42.2],
                        ['Septiembre', 32.0],
                        ['Octubre', 41.7],
                        ['Noviembre', 39.5],
                        ['Diciembre', 50.2],
                        
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '8px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
                
            });
            
            function carrucel()
            {
                
                $.post( "<?php echo base_url(); ?>welcome/carrucel/", { }).done(function( data ) 
                {
                    $("#carrucel").html(data);
                });
            }
            
            
            
            
            
        </script>
        
        
