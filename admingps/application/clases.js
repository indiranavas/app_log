jQuery(function($) {			
	$('#form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					ignore: "",
					rules: {
						clasificacion: {
							required: true
						},
						observa: {
							required: true
						}
					},
			
					messages: {
						clasificacion: "Por favor seleccione una opción",
						observa: "Por favor escriba alguna Observación"
					},
			
			
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
					},
					invalidHandler: function (form) {
					}
				});
})
var tablaactual = document.getElementById("tabla").value;
var modulo = document.getElementById("modulo").value;
var base_url = '<?php echo base_url(); ?>';
alert(modulo);
function bt_salir(){
	/* var tablaactual = "<?php echo $tabla; ?>"; */
var modulo = "<?php echo $modulo; ?>";
var base_url = '<?php echo base_url(); ?>';
$('#Info').fadeIn(5000).html('');
var myCallback = function(choice){
    if(choice){
        notif({
            'type': 'success',
            'msg': 'Saliendo...',
			'autohide':false,
			'width':'all',
            'position': 'right'
        });
		var base_url = '<?php echo base_url(); ?>';
		var pagina = base_url+modulo;	
		location.href=pagina;

		return false;   
    }else{
        notif({
            'type': 'error',
            'msg': 'Regresando...',
            'position': 'center'
        });
		return;
    }
}

notif_confirm({
	'textaccept': 'Si',
	'textcancel': 'No',
	'fullscreen': true,
	'message': '¿Está Seguro de Salir?',
	'callback': myCallback
})
}