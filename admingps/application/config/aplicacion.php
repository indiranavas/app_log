<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| APLICACION CONFIG
| -------------------------------------------------------------------
| This file will contain some 'aplicacion' settings.
|
| $config['title']				Titulo de la pagina.
| $config['subtitle'] 			Subtitulo de la pagina.
| $config['pie'] 				Lo que se muestra al pie de la pagina.
| $config['ie'] 				Version minima de internet explorer soportada por el sistema.
| $config['interval']			Intervalo de tiempo en milisegundos para configurar la recarga de pagina o elemento
| $config['fontsize']			Tamaño en PX de la fuente de los graficos
|
*/

$config['title'] = 'Log Solutions';
$config['subtitle'] = '';
$config['pie'] = 'Log Solutions';
$config['ie'] = 9;
$config['interval'] = 300000;
$config['fontsize'] = 14;
$config['menu_top'] = '';


/* End of file aplicacion.php */
/* Location: ./application/config/aplicacion.php */