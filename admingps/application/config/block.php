<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| BLOCK CONFIG
| -------------------------------------------------------------------
| This file will contain some 'block' settings.
|
| $config['block']				Cuando BLOCK es TRUE el sistema está cerrado, cuando es FALSE el sistema está abierto .
| $config['ips'] 				Array de Ip de excepcion de bloqueo.
| $config['error_controller'] 	Controller que maneja al error.
|
|
| Nota: esta libreria requiere el helper 'url' activado y la variable $config['base_url'] correctamente configurada para funcionar.
*/

$config['block'] = FALSE;
$config['ips'] = array('127.0.0.1','130.0.5.178');
$config['error_controller'] = 'error_block';


/* End of file block.php */
/* Location: ./application/config/block.php */