<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('showfolio'))
{
	/**
	 * [showfolio crea un folio unico en momento de ejecucion]
	 * @return [md5] [string con el dato encriptado]
	 */
	function showfolio(){
		usleep(1);
		$time = microtime(true);
		$encript = md5($time);
		return $encript;
	}
}

//end application/helpers/general_helper.php