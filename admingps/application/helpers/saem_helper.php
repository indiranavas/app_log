<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//si no existe la función invierte_date_time la creamos
if(!function_exists('invierte_date_time'))
{
    //formateamos la fecha y la hora, función de cesarcancino.com
	function invierte_date_time($fecha)
	{

		$day=substr($fecha,8,2);
		$month=substr($fecha,5,2);
		$year=substr($fecha,0,4);
		$hour = substr($fecha,11,5);
		$datetime_format=$day."-".$month."-".$year.' '.$hour;
		return $datetime_format;

	}
}

if(!function_exists('get_users'))
{
	function get_users()
	{
        //asignamos a $ci el super objeto de codeigniter
		//$ci será como $this
		$ci =& get_instance();
		$query = $ci->db->get('usuarios');
		return $query->result();

	}
}

if(!function_exists('depositos_stocks'))
{
    //formateamos la fecha y la hora, función de cesarcancino.com
	function depositos_stocks(){
		$ci =& get_instance();
		$departa = $ci->funciones->retorna_depositos();
		
		$optiondepa = "<option value=''>Seleccione una Opción...</option>";
		foreach($departa -> result() as $fila) {
			$optiondepa .= "<option value='$fila->iddepositos'>$fila->nombre</option>";			
		}
		
		return $optiondepa;	 
	}
}

if(!function_exists('enviar_correo'))
{
      function enviar_correo($nombre,$correo,$asunto,$cuerpo) {
		$ci =& get_instance();
        /*  */
        $mail = new PHPMailer();
        $mail->IsSMTP(); // establecemos que utilizaremos SMTP
        $mail->SMTPAuth   = true; // habilitamos la autenticación SMTP
        $mail->SMTPSecure = "ssl";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
		/* $mail->SMTPOptions = array('ssl' => array('verify_peer' => false,
												  'verify_peer_name' => false,
												  'allow_self_signed' => true
													)); */
        $mail->Host       = "usm1.noc41.com";      // establecemos GMail como nuestro servidor SMTP
        $mail->Port       = 465;                   // establecemos el puerto SMTP en el servidor de GMail
        $mail->Username   = "no-responder@mercadoreino.com.ve";  // la cuenta de correo GMail
        $mail->Password   = "av112016av";            // password de la cuenta GMail
        $mail->SetFrom('no-responder@mercadoreino.com.ve', utf8_decode('VetMedic Clínica Veterinaria'));  //Quien envía el correo
        $mail->AddReplyTo("aemogar@gmail.com","Angel Mogollón");  //A quien debe ir dirigida la respuesta
        $mail->Subject    = $asunto;  //Asunto del mensaje
        $mail->Body      =$cuerpo;
        $mail->AltBody    = "Cuerpo en texto plano";
        $destino = "aemogar@gmail.com";
        $mail->AddAddress($correo,$nombre);
		/* $mail->AddAttachment($logos); */

        /* $mail->AddAttachment("images/phpmailer.gif");   */    // añadimos archivos adjuntos si es necesario
       /*  $mail->AddAttachment("images/phpmailer_mini.gif"); */ // tantos como queramos

        if(!$mail->Send()) {
            $data["message"] = "Error en el envío: " . $mail->ErrorInfo;
			 $mail->ClearAddresses();
			/* $ci->funciones->policia($mail->ErrorInfo); */
        } else {
            $data["message"] = "¡Mensaje enviado correctamente!";
			 $mail->ClearAddresses();
        }
		return true;
        /* $this->load->view('Vs_enviarcorreo',$data); */
    } 
}	

if(!function_exists('policia'))
{
    function policia($texto) {
	$nombre_fichero = 'fichero.txt';
	if (file_exists($nombre_fichero)) {
		unlink($nombre_fichero);
	}
		$fp = fopen("fichero.txt", "w");
		fputs($fp, $texto);
		fclose($fp);
	}
}	
if(!function_exists('montar_menu'))
{
    function montar_menu(){
	$ci =& get_instance();
	$datos['menuprin0']=$ci->Md_menues->abrir_menu(0,0);
	
	return $datos;
	}
}	

if(!function_exists('createFolder'))
{	
	function createFolder()
    {
        if(!is_dir("./files"))
        {
            mkdir("./files", 0777);
            mkdir("./files/pdfs", 0777);
        }
    }
}	
//end application/helpers/ayuda_helper.php