<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Template Class
 *
 * Esta clase carga los templates predefinidos para incorporar vistas
 *
 * @package		Template
 * @version		1.0
 * @author 		Giovanni La mura
 * @copyright 	Copyright (c) 2014
 */
class Template {
		var $template_data = array();

		public function __construct()
		{
    	    $this->ci =& get_instance();
    	    $this->ci->load->library(array('breadcrumbs','fecha'));
    	    $this->ci->breadcrumbs->unshift("<img src='".base_url()."assets/images/Home-icon.png' alt='Home' />", '/');
		}
		
		function set($name, $value)
		{
			$this->template_data[$name] = $value;
		}
	
		function load($template = '', $cssjs = '' , $view = '' , $view_data = array(), $return = FALSE)
		{               

			 if ($cssjs != '')
			 	{$this->set('cssjs', $this->ci->load->view($cssjs, '', TRUE));}
			 else
			 	{$this->set('cssjs','');}

			$this->set('contents', $this->ci->load->view($view, $view_data, TRUE));		

			return $this->ci->load->view($template, $this->template_data, $return);

		}
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */