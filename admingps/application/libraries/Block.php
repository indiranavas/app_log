<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Block Class
 *
 * Esta clase controla el bloqueo de la aplicación mediante su propia configuración
 * exceptuando las IP descritas
 *
 * @package		Block
 * @version		1.0
 * @author 		Giovanni La mura
 * @copyright 	Copyright (c) 2013
 */
class Block
{
  protected 	$ci;

	public function __construct()
	{
        $this->ci =& get_instance();
        // Load config file
		$this->ci->load->config('block');
		// Get block display options
		$this->block = $this->ci->config->item('block');
		$this->ips = $this->ci->config->item('ips');
		$this->err = $this->ci->config->item('error_controller');
		$this->url = str_replace('index.php/', '', current_url());

		if ($this->load() == FALSE) {
			if ($this->url != base_url($this->err)) {
				redirect(base_url($this->err));
			}
			
		}else{
			if ($this->url == base_url($this->err)) {
				redirect(base_url());
			}
		}
	}

	public function load()
	{
		if ($this->block == TRUE) {
			foreach ($this->ips as $ip) {
				if ($ip == $_SERVER['REMOTE_ADDR']) {
					return TRUE;
				}
			}
			return FALSE;
		}else{
			return TRUE;
		}
	}

}

/* End of file Block.php */
/* Location: ./application/libraries/Block.php */
