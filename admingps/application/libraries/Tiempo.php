<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tiempo Class
 *
 * Esta clase contiene metodos estaticos de control de tiempos
 *
 * @package     Tiempo
 * @version     1.0
 * @author      Giovanni La mura
 * @copyright   Copyright (c) 2015
 */
class Tiempo
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

    /**
     * [gettiempo crea dias, horas, minutos, y segundos dependiendo del tiempo definido en la variable de entrada]
     * @param  [int] $time    [numero que contiene la cantidad de horas, minutos o segundos para setear]
     * @param  string $formato [define el formato de entrega de la variable "time" 'h', 'm', 's']
     * @return [array]          [retorna dias, horas, minutos y segundos dependiendo el calculo de las variable de entrada]
     */
	public static function gettiempo($time, $formato = 'h'){
        if ($formato == 'h') {
            $timesegundos = ($time * 3600);
        }elseif ($formato == 'm') {
            $timesegundos = ($time * 60);
        }else{
            $timesegundos = $time;
        }

        $salida = array();

        $timedias = floor($timesegundos / 86400);
        $salida['dias'] = floor($timedias);

        $timesegundos = ($timesegundos - ($salida['dias'] * 86400));
        $salida['horas'] =  floor($timesegundos / 3600);

        $timesegundos = ($timesegundos - ($salida['horas'] * 3600));
        $salida['minutos'] = floor($timesegundos / 60);

        $timesegundos = ($timesegundos - ($salida['minutos'] * 60));
        $salida['segundos'] = floor($timesegundos); 

        return $salida;
    }

    /**
     * [getformatcero crea formato de fecha con ceros a la izquierda de numeros menores a 10]
     * @param  [array] $time [recibe la salida de la función gettiempo]
     * @return [array]       [retorna dias, horas, minutos y segundos]
     */
    public static function getformatcero($time)
    {
    	$salida['dias'] = $time['dias'];
		$salida['horas'] = ($time['horas'] < 10) ? '0'.$time['horas'] : $time['horas'];
		$salida['mins'] = ($time['minutos'] < 10) ? '0'.$time['minutos'] : $time['minutos'];
		$salida['sec'] = ($time['segundos'] < 10) ? '0'.$time['segundos'] : $time['segundos'];

		return $salida;
    }

    /**
     * [getdiferenciaenhoras diferencia entre 2 datetimes]
     * @param  [datetime] $time1 [datetime mayor]
     * @param  [datetime] $time2 [datetime menor]
     * @return [float]        [diferencia calculada entre ambos datetimes]
     */
    public static function getdiferenciaenhoras($time1,$time2)
    {

    	$diferencia = $time1->diff($time2);

		$d = $diferencia->format('%D') * 24;
		$h = $diferencia->format('%H');
		$m = $diferencia->format('%I') / 60;
		$s = $diferencia->format('%S') / 3600;

		return $d+$h+$m+$s;
    }

    /**
     * [transformarleadtime transforma el formato hora de leadtime a cantidad de horas]
     * @param  [time] $time [leadtime formato: 00:00:00]
     * @return [int]       [cantidad de horas]
     */
    public static function transformarleadtime($time)
    {
    	if (is_numeric($time)) {
    		return $time;
    	}

    	$leadtime = explode(":",$time);

    	$salida = $leadtime[0]+($leadtime[1]/60)+($leadtime[2]/3600);
		return $salida;
    }
    public static function dma_hm($fecha)
    {
        $a = substr($fecha, 0, 4);
        $m = substr($fecha, 5, 2);
        $d = substr($fecha, 8, 2);
        $h = substr($fecha, 11, 2 );
        $i = substr($fecha, 14, 2 );
        return $d."-".$m."-".$a." ".$h.":".$i;
    }

}

/* End of file Tiempo.php */
/* Location: ./application/libraries/Tiempo.php */
