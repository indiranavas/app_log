<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Fecha Class
 *
 * Esta clase crea y retorna formatos especificos de fecha en español
 *
 * @package		Fecha
 * @version		1.2
 * @author 		Giovanni La mura
 * @copyright 	Copyright (c) 2014 - 2015
 */
class Fecha
{
  protected 	$ci;
  private 		$fecha;
  private 		$ndia;
  private 		$dia;
  private 		$nmes;
  private 		$mes;
  private 		$anio;

	public function __construct($fech = '',$form = '')
	{
        $this->ci =& get_instance();
        $fechSal = $this->transformar($fech,$form);
        $this->fecha = new DateTime($fechSal);
        $this->load();
	}

	private function load(){
		// Obtenemos y traducimos el nombre del día
		$this->ndia=$this->fecha->format("l");
		if ($this->ndia=="Monday") $this->ndia="Lunes";
		if ($this->ndia=="Tuesday") $this->ndia="Martes";
		if ($this->ndia=="Wednesday") $this->ndia="Mi&eacute;rcoles";
		if ($this->ndia=="Thursday") $this->ndia="Jueves";
		if ($this->ndia=="Friday") $this->ndia="Viernes";
		if ($this->ndia=="Saturday") $this->ndia="S&aacute;bado";
		if ($this->ndia=="Sunday") $this->ndia="Domingo";
	 
		// Obtenemos el número del día
		$this->dia=$this->fecha->format("d");

		// Obtenemos el número del mes
		$this->nmes=$this->fecha->format("m");
	 
		// Obtenemos y traducimos el nombre del mes
		$this->mes=$this->fecha->format("F");
		if ($this->mes=="January") $this->mes="Enero";
		if ($this->mes=="February") $this->mes="Febrero";
		if ($this->mes=="March") $this->mes="Marzo";
		if ($this->mes=="April") $this->mes="Abril";
		if ($this->mes=="May") $this->mes="Mayo";
		if ($this->mes=="June") $this->mes="Junio";
		if ($this->mes=="July") $this->mes="Julio";
		if ($this->mes=="August") $this->mes="Agosto";
		if ($this->mes=="September") $this->mes="Setiembre";
		if ($this->mes=="October") $this->mes="Octubre";
		if ($this->mes=="November") $this->mes="Noviembre";
		if ($this->mes=="December") $this->mes="Diciembre";
	 
		// Obtenemos el año
		$this->anio=$this->fecha->format("Y");
	}

	public function show($format=0){

		switch ($format) {
			case 0:
				return $this->dia."/".$this->nmes."/".$this->anio;
				break;

			case 1:
				return $this->dia."-".$this->nmes."-".$this->anio;
				break;
			
			case 2:
				return "$this->dia de $this->mes de $this->anio";
				break;

			case 3:
				return "$this->ndia, $this->dia de $this->mes de $this->anio";
				break;

			case 4:
				return $this->anio."-".$this->nmes."-".$this->dia;
				break;

			case 5:
				return $this->anio."-".$this->nmes;
				break;

			default:
				break;
		}
	}

	function transformar($vfech,$form = ''){
		if (!empty($form)) {
			$form = strtoupper($form);
			$retForm = explode('/',$form);
			$separ = "/";
			if (!isset($retForm[1])) {
				$retForm = explode('-',$form);
				$separ = "-";
			}

			$retFecha = explode($separ,$vfech);
			switch ($retForm[0]) {
				case 'D':
					$dd = $retFecha[0];
					break;
				case 'DD':
					$dd = $retFecha[0];
					break;
				case 'M':
					$mm = $retFecha[0];
					break;
				case 'MM':
					$mm = $retFecha[0];
					break;
				case 'Y':
					$yyyy = $retFecha[0];
					break;
				case 'YYYY':
					$yyyy = $retFecha[0];
					break;
				default:
					break;
			}

			switch ($retForm[1]) {
				case 'D':
					$dd = $retFecha[1];
					break;
				case 'DD':
					$dd = $retFecha[1];
					break;
				case 'M':
					$mm = $retFecha[1];
					break;
				case 'MM':
					$mm = $retFecha[1];
					break;
				case 'Y':
					$yyyy = $retFecha[1];
					break;
				case 'YYYY':
					$yyyy = $retFecha[1];
					break;
				default:
					break;
			}

			switch ($retForm[2]) {
				case 'D':
					$dd = $retFecha[2];
					break;
				case 'DD':
					$dd = $retFecha[2];
					break;
				case 'M':
					$mm = $retFecha[2];
					break;
				case 'MM':
					$mm = $retFecha[2];
					break;
				case 'Y':
					$yyyy = $retFecha[2];
					break;
				case 'YYYY':
					$yyyy = $retFecha[2];
					break;
				default:
					break;
			}

			$vfech = $yyyy."-".$mm."-".$dd;
		}

		return $vfech;
	}

}

/* End of file Fecha.php */
/* Location: ./application/libraries/Fecha.php */
