<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Send_mail Class
 *
 * Esta clase controla y predefine la configuración de la librería "mail" de codeigniter
 *
 * @package		Send_mail
 * @version		1.0
 * @author 		Giovanni La mura
 * @copyright 	Copyright (c) 2014
 */
class Send_mail
{
  protected 	$ci;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->config('send_mail');
        $this->ci->load->library('email');

        $data = array('mailtype' => 'html',
					  'charset' => 'utf-8',
					  'newline' => '\r\n');
		$this->ci->email->initialize($data);
        $this->ci->email->from($this->ci->config->item('mail_from'), $this->ci->config->item('mail_title'));	
	}

	public function enviar($para,$asunto = '',$mensaje = ''){
		
		$this->ci->email->to($para); 
		$this->ci->email->subject($asunto);
		$this->ci->email->message($mensaje);	
		
		$this->ci->email->send();

		return $this->ci->email->print_debugger();
	}
}

/* End of file Send_mail.php */
/* Location: ./application/libraries/Send_mail.php */
