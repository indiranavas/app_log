<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."third_party".DIRECTORY_SEPARATOR."PHPExcel.php";
/**
 * Esta clase se extiende de PHPExcel para ser utilizada en el sistema
 */
class Excel extends PHPExcel
{

	public function __construct()
	{
        $this->ci =& get_instance();
	}

}

/* End of file Excel.php */
/* Location: ./application/libraries/Excel.php */
