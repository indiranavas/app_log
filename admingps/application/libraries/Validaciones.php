<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Validaciones Class
 *
 * Esta clase contiene metodos de validaciones necesarias
 *
 * @package     Validaciones
 * @version     1.5
 * @author      Giovanni La mura
 * @copyright   Copyright (c) 2013-2015
 */
class Validaciones
{
  protected 	$ci;

    /**
     * [ValidaPerfil redireccionamiento al cierre de sesión cuando el 
     * perfil de sesion no concuerda con el array de perfiles permitidos]
     * @param [array] $arrayid [array de perfiles permitidos]
     */
    public function ValidaPerfil($arrayid){
        $this->ci =& get_instance();
        $perfil = (int) $this->ci->session->userdata('perfil');

        if (!in_array($perfil, $arrayid)) {
            redirect(base_url('login/logout'));
        }

    }

    /**
     * [ValidaDVRut validación de rut sin digito verificador]
     * @param [string] $setrut [rut sin digito verificador ni puntos]
     * @return [string]      [digito verificador del rut seteado]
     */
	public function ValidaDVRut($setrut) {
		$this->ci =& get_instance();

		if (trim($setrut) == '') {return '';}
 		
    	$tur = strrev($setrut);
    	$mult = 2;
    	$suma = 0;
 	
    	for ($i = 0; $i <= strlen($tur); $i++) { 
    	   if ($mult > 7) $mult = 2; 
 	
    	   $suma = $mult * substr($tur, $i, 1) + $suma;
    	   $mult = $mult + 1;
    	}
 	
    	$valor = 11 - ($suma % 11);
 	
    	if ($valor == 11) { 
    	    $codigo_veri = "0";
    	  } elseif ($valor == 10) {
    	    $codigo_veri = "K";
    	  } else { 
    	    $codigo_veri = $valor;
    	}
  		return $codigo_veri;
	}

    /**
     * [FormatRut transformar rut sin puntos ni guión a rut con puntos y guión]
     * @param [string] $rut_param [rut sin puntos ni guión]
     * @return [string]      [cadena de texto transformada]
     */
    public function FormatRut($rut_param){
        
        $parte4 = substr($rut_param, -1); // seria solo el numero verificador 
        $parte3 = substr($rut_param, -4,3); // la cuenta va de derecha a izq  
        $parte2 = substr($rut_param, -7,3);  
        $parte1 = substr($rut_param, 0,-7); //de esta manera toma todos los caracteres desde el 7 hacia la izq 

        return $parte1.".".$parte2.".".$parte3."-".$parte4;
    }

    /**
     * [fullLower transformar string con tildes a minuscula]
     * @param  [string] $str [cadena de texto a transformar a minusculas]
     * @return [string]      [cadena de texto transformada]
     */
    public static function fullLower($str){
        
        $str = str_replace("Á", "á", $str); 
        $str = str_replace("É", "é", $str); 
        $str = str_replace("Í", "í", $str); 
        $str = str_replace("Ó", "ó", $str); 
        $str = str_replace("Ú", "ú", $str);
        $str = str_replace("Ñ", "ñ", $str);
       
       return(strtolower($str));
    }

     /**
     * [fullUpper transformar string con tildes a mayusculas]
     * @param  [string] $str [cadena de texto a transformar a mayuscular]
     * @return [string]      [cadena de texto transformada]
     */
    public static function fullUpper($str){
        
        $str = str_replace("á", "Á", $str); 
        $str = str_replace("é", "É", $str); 
        $str = str_replace("í", "Í", $str); 
        $str = str_replace("ó", "Ó", $str); 
        $str = str_replace("ú", "Ú", $str);
        $str = str_replace("ñ", "Ñ", $str);
       
       return(strtoupper($str));
    }

    /**
     *  Ordena arreglos
     * @param array $ArrayaOrdenar 
     * @param string $ArrayCampo, campo por el cual se quiere ordenar el array 
     * @param string $typeorder, tipo de orden: desc o asc 
     * @return array
     */
    function OrdernarArray ($ArrayaOrdenar, $ArrayCampo, $typeorder = 'desc') {
        $Array = array();
        $NuevaFila = array();
        $ArrayContador = 0;

        foreach ($ArrayaOrdenar as $key => $value) {
            if (!isset($value[$ArrayCampo])) {
                return $ArrayaOrdenar;
            }
        }
        
        foreach ($ArrayaOrdenar as $clave => $fila) {
            $Array[$clave] = $fila[$ArrayCampo];
            $NuevaFila[$clave] = $fila;
        }
        
        if ($typeorder == 'asc'){
            arsort($Array);
        }else{
            asort($Array);
        }

        $ArrayOrdenado = array();
        
        foreach ($Array as $clave => $pos) {
            $ArrayOrdenado[] = $NuevaFila[$clave];
        }
        
        return $ArrayOrdenado;
    }

    /**
     *  Todos los datos mayusculas, minusculas o primera letra mayuscula y la demas minusculas (estructura row o multi, sin mezclar)
     * @param array $setArray 
     * @param string $StrTo , propiedad de array_map, 
     * strtoupper: todo el texto en mayusculas, 
     * strtolower: todo el texto en minusculas, 
     * ucfirst: Convierte el primer caracter a mayúsculas, 
     * ucwords: Convierte a mayúsculas el primer caracter de cada palabra
     * @return array 
     */
    public static function ArrayMap ($setArray,$StrTo = 'strtoupper'){
        $array = array();
        $keys = array_keys($setArray);

        if (!is_array($keys[0])) {
            if($StrTo == 'ucfirst' || $StrTo == 'ucwords'){
                $setArray = array_map('strtolower', $setArray);
            }

            $array = array_map($StrTo, $setArray);

            if ($StrTo == 'strtoupper'){
                foreach ($array as $llave => $valor){
                    $array[$llave] = str_replace("á", "Á", $array[$llave]); 
                    $array[$llave] = str_replace("é", "É", $array[$llave]); 
                    $array[$llave] = str_replace("í", "Í", $array[$llave]); 
                    $array[$llave] = str_replace("ó", "Ó", $array[$llave]); 
                    $array[$llave] = str_replace("ú", "Ú", $array[$llave]);
                    $array[$llave] = str_replace("ñ", "Ñ", $array[$llave]);
                }
            }

            if ($StrTo == 'strtolower' || $StrTo == 'ucfirst' || $StrTo == 'ucwords'){
                foreach ($array as $llave => $valor){
                    $array[$llave] = str_replace("Á", "á", $array[$llave]); 
                    $array[$llave] = str_replace("É", "é", $array[$llave]); 
                    $array[$llave] = str_replace("Í", "í", $array[$llave]); 
                    $array[$llave] = str_replace("Ó", "ó", $array[$llave]); 
                    $array[$llave] = str_replace("Ú", "ú", $array[$llave]);
                    $array[$llave] = str_replace("Ñ", "ñ", $array[$llave]);
                }
            }
        }else{
            if($StrTo == 'ucfirst' || $StrTo == 'ucwords'){
                foreach ($setArray as $key => $value) {
                    $setArray[$key] = array_map('strtolower', $setArray[$key]);
                }
            }
    
            foreach ($setArray as $key => $value) {
                $array[$key] = array_map($StrTo, $setArray[$key]);
    
                if ($StrTo == 'strtoupper'){
                    foreach ($array[$key] as $llave => $valor){
                        $array[$key][$llave] = str_replace("á", "Á", $array[$key][$llave]); 
                        $array[$key][$llave] = str_replace("é", "É", $array[$key][$llave]); 
                        $array[$key][$llave] = str_replace("í", "Í", $array[$key][$llave]); 
                        $array[$key][$llave] = str_replace("ó", "Ó", $array[$key][$llave]); 
                        $array[$key][$llave] = str_replace("ú", "Ú", $array[$key][$llave]);
                        $array[$key][$llave] = str_replace("ñ", "Ñ", $array[$key][$llave]);
                    }
                }
    
                if ($StrTo == 'strtolower' || $StrTo == 'ucfirst' || $StrTo == 'ucwords'){
                    foreach ($array[$key] as $llave => $valor){
                        $array[$key][$llave] = str_replace("Á", "á", $array[$key][$llave]); 
                        $array[$key][$llave] = str_replace("É", "é", $array[$key][$llave]); 
                        $array[$key][$llave] = str_replace("Í", "í", $array[$key][$llave]); 
                        $array[$key][$llave] = str_replace("Ó", "ó", $array[$key][$llave]); 
                        $array[$key][$llave] = str_replace("Ú", "ú", $array[$key][$llave]);
                        $array[$key][$llave] = str_replace("Ñ", "ñ", $array[$key][$llave]);
                    }
                }               
            } 
        }

        return $array;
    }

    /**
     *  Limpiar datos de un arreglo
     * @param array $setArray 
     * @return array 
     */
    public static function ArrayClear ($setArray){
        $Array = array();

        foreach ($setArray as $key => $value) {
            if (is_array($value)) {
                $array[$key] = array_map('trim', $value);
            }else{
                $array[$key] = trim($value);
                if ($array[$key] == null || $array[$key] == 'null' ||  $array[$key] == 'NULL'){
                    $array[$key] = "";
                }
            }
        }

        return $array;
    }

    public static function dv($r) {  //obtiene el digito verificador
       $s = 1; 
       for($m = 0; $r != 0; $r/= 10)$s = ($s+$r%10 * (9-$m++%6))%11; 
       return chr($s?$s+47:75); 
     }

}

/* End of file Validaciones.php */
/* Location: ./application/libraries/Validaciones.php */
