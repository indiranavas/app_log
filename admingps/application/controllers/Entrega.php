<?php
defined('BASEPATH') OR exit('No permitir el acceso directo al script');
class Entrega extends CI_Controller 
{

  function __construct(){
        parent::__construct();


  }
  
  public function index(){
  //Datos para el sidebar y navbar
    $datos['activar_menu'] = "entrega"; 
    $datos['currentPage'] ='dashboard';


    //Vistas 
    $this->load->view('head.php'); 
    $this->load->view('navbar.php',$datos);
    $this->load->view('sidebar/entrega.php');
    $this->load->view('entrega/dashboard');
    $this->load->view('footer.php'); 
      }



  public function dashboard() 
  {
  
    //Datos para el sidebar y navbar
    $datos['activar_menu'] = "entrega"; 
    $datos['currentPage'] ='dashboard';


    //Vistas 
    $this->load->view('head.php'); 
    $this->load->view('navbar.php',$datos);
    $this->load->view('sidebar/entrega.php');
    $this->load->view('entrega/dashboard');
    $this->load->view('footer.php'); 
  }


}
